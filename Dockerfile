FROM python:3-alpine
RUN pip install prometheus_client
COPY openmetrics.py .
CMD [ "python", "./openmetrics.py" ]