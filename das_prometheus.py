#!/usr/bin/python

from prometheus_client import Gauge, start_http_server

import random
import time
import logging

# https://medium.com/@valyala/promql-tutorial-for-beginners-9ab455142085
# https://github.com/prometheus/client_python
def app_check():
    """mock for  application response"""
    s = """{\"status\":\"UP\",\"widget\":{\"status\":\"UP\",\"widgetName\":test}
        """
    return s





if __name__ == "__main__":

    g = Gauge('my_app','Description of gauge')

    start_http_server(8888)
    

    while True:
        g.set(0)
        time.sleep(2)
        g.inc()
        time.sleep(2)
        g.dec()




    




   
