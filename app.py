import random, time

from flask import Flask, render_template_string, abort
from prometheus_client import generate_latest, REGISTRY, Counter, Gauge, Histogram


app = Flask(__name__)

# Metrics type INSTANTIATION aka naming metrics in here
REQUESTS = Counter('http_requests_total','Total HTTP Requests (count)', ['method','endpoint','status_code'])

# c = Counter('my_requests_total', 'HTTP Failures', ['method', 'endpoint'])
#  |          c.labels(method='get', endpoint='/').inc()
#  |          c.labels(method='post', endpoint='/submit').inc()

# increment/decrement when enter/leave method scope 
IN_PROGRESS = Gauge('http_requests_total_inprogress','Number of in progress HTTP requests')

# measure how long a method takes
TIMINGS = Histogram('http_requests_duration_seconds','HTTP request latency (seconds)')

@app.route('/')
@TIMINGS.time()
@IN_PROGRESS.track_inprogress()
def hello_world():
    """ increment every time when user hits home"""
    #increment counter when GET methods is served OK
    REQUESTS.labels(method='GET', endpoint='/', status_code=200).inc() 
    return "WAAASUUP"

@app.route('/hello/<name>')
@TIMINGS.time()
@IN_PROGRESS.track_inprogress()
def index(name):
    """ get params from url """
    #increment counter when GET methods is served OK
    REQUESTS.labels(method='GET', endpoint='/hello/dej', status_code=200).inc() 
    return render_template_string('<b> Welcome Home {{name}} man! </b>', name=name)

@app.route('/metrics')
@IN_PROGRESS.track_inprogress()
@TIMINGS.time()
def metrics():
    """expose metrics to prometheus """
    REQUESTS.labels(method="GET", endpoint="/metrics", status_code=200).inc()
    return generate_latest(REGISTRY)


if __name__ == "__main__":
    app.run(host='0.0.0.0')




 