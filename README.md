# monitoRINGZ

**Prometheus image**:

bind mount the `prometheus.yml` from host:

```bash
$ docker run -d -p 9090:9090 \
            --name localprometheus \
            -v absolute/path/to/your/prometheus.yml:/etc/prometheus/prometheus.yml
            prom/prometheus
```
    
bind mount the directory containing `prometheus.yml` onto `/etc/prometheus`:


**Grafana image**:
```bash
$ docker run -d -p 3000:3000 \
           --name localgrafana \
           -v absolute/path/to/your/local/grafana.ini:/etc/grafana/grafana.ini \
           -v grafana-storage:/var/lib/grafana \
            grafana/grafana`
```
Create datasource for grafana:
`$ docker cp datasource.yml localgrafana:/etc/grafana/provisioning/datasources/datasource.yml`
`$ docker restart localgrafana` 

---

* Docker compose approach:  


`$ docker-compose -f docker-compose.yml config`  

`$ docker-compose up --remove-orphans`

----

* Get image with vanilla http server
`docker pull dejanualex/python_push_metrics`
------------
```

If the reload is successful Prometheus will log that it has updated its targets
must enable Lifecycle API is not enabled. --web.enable-lifecycle

promready='curl http://localhost:9091/-/ready'
promhealth='curl http://localhost:9091/-/healthy'
promreload='curl -X POST http://localhost:9091/-/reload'
```
